package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

// struct for profile
type profile struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Age         int    `json:"age"`
	Address     string `json:"address"`
	Email       string `json:"email"`
	PhoneNumber int    `json:"phonenumber"`
	Hobby       string `json:"hobby"`
}

var myProfile = []profile{
	{Id: 1, Name: "Phurba", Age: 24, Address: "Gairapata-4, Pokhara", Email: "amgineenigma9@gmail.com", PhoneNumber: 9806797664, Hobby: "Reading Books"},
	{Id: 2, Name: "Bhanu", Age: 27, Address: "Nadipur-3, Pokhara", Email: "bhanulamichhane@gmail.com", PhoneNumber: 9809807654, Hobby: "Watching News"},
	{Id: 3, Name: "Sandip", Age: 20, Address: "Bagar-9, Pokhara", Email: "Sandeep123@gmail.com", PhoneNumber: 9806664421, Hobby: "Gaming"},
}

// All http request is handled under http function
func httpRequest() {
	router := gin.Default()
	router.GET("/profile", getProfile)
	router.POST("/profile", postProfile)
	router.GET("/profile/:name", searcProfilebyName)
	router.GET("/profile/:id", func (c *gin.Context) {
		idString := c.Param("id")
		idInt, err := strconv.Atoi(idString)
	
		if err != nil {
			panic(err)
		}
	
		for i, p := range myProfile {
			if myProfile[i].Id == idInt {
				c.IndentedJSON(http.StatusOK, p)
				return
			}
		}
		c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Yes Not found"})
	}
	)
	router.Run("localhost:9090")

}

//getting profile in json format
func getProfile(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, myProfile)
}

//binding json and adding to slice
func postProfile(c *gin.Context) {
	var newProfile profile
	if err := c.BindJSON(&newProfile); err != nil {
		return
	}
	insertData(newProfile)
	c.IndentedJSON(http.StatusCreated, newProfile)
}

// to retrieve data from database and direct it to the server
func searcProfilebyName(c *gin.Context) {
	psqlInfo := fmt.Sprintf(`host=%s port=%d dbname=%s user=%s 
	password=%s sslmode=disable`, host, port, dbname, user, password) // conection string
	db, err := sql.Open("postgres", psqlInfo) //approving connection
	if err != nil {
		panic(err)
	}
	defer db.Close()
	err = db.Ping() // checking if connection is seccesful
	if err != nil {
		panic(err)
	}
	fmt.Println("Successfully Connected")

	name := c.Param("name") //parameter sent by user
	var rowData profile     // to store data for single user
	sqlStatements := `SELECT id, name, age, address, email,phoneNumber, hobby
	                    FROM interns
	 				   WHERE name=$1;` // Query string
	data := db.QueryRow(sqlStatements, name) // Executing Query
	//db.Qery() return err at the time of row Scan
	// data has details about single user  retrieve from database and it is stored in rowData
	//case nil means data succesfully retrieved
	if err := data.Scan(&rowData.Id, &rowData.Name, &rowData.Age, &rowData.Address,
		&rowData.Email, &rowData.PhoneNumber, &rowData.Hobby); err != nil {
		panic(err)
	} else if err == nil {
		c.IndentedJSON(http.StatusOK, rowData)
		return

	} else {
		c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Not found"})

	}
}

//Search profile by Id
func searcProfilebyId(c *gin.Context) {
	idString := c.Param("id")
	idInt, err := strconv.Atoi(idString)

	if err != nil {
		panic(err)
	}

	for i, p := range myProfile {
		if myProfile[i].Id == idInt {
			c.IndentedJSON(http.StatusOK, p)
			return
		}
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Yes Not found"})
}
func displayDB(db *sql.DB) {
	sqlStatements := `SELECT * FROM interns`
	rows, err := db.Query(sqlStatements)
	fmt.Println("<<<<<<<<<<<<<<<<<<")
	fmt.Println("", rows)
	fmt.Println("<<<<<<<<<<<<<<<<<<<<<")
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		var (
			id          int
			age         int
			name        string
			address     string
			email       string
			phoneNumber int
			hobby       string
		)
		if err := rows.Scan(&id, &age, &name, &address, &email, &phoneNumber, &hobby); err != nil {
			log.Fatal(err)
		}
		log.Printf("id : %d age :  %d name :  %s  address: %s email: %s phone Number: %d  hobby : %s\n ", id, age, name, address, email, phoneNumber, hobby)
	}

}
func update(db *sql.DB) {
	sqlStatement := `UPDATE interns
                     SET name = $2, email = $3
                     WHERE age = $1;`
	_, err := db.Exec(sqlStatement, 24, "Enigma", "abc@gmail.com")
	if err != nil {
		panic(err)
	}
}
func delete(db *sql.DB) {
	sqlStatement := `
DELETE FROM interns
WHERE age = $1;`
	_, err := db.Exec(sqlStatement, 24)
	if err != nil {
		panic(err)
	}
}

func insertData(newProfile profile) {
	psqlInfo := fmt.Sprintf(`host=%s port=%d dbname=%s password=%s user=%s sslmode=disable`, host, port, dbname, password, user)
	db, err := sql.Open("postgres", psqlInfo)

	if err != nil {
		panic(err)
	}
	defer db.Close()
	if err = db.Ping(); err != nil {
		panic(err)
	}
	fmt.Println("Connection Succesful")
	sqlStatements := `INSERT INTO interns ( id, name, age, address, email, phonenumber, hobby)
		VALUES ($1, $2,$3 , $4, $5, $6, $7)`
	rest, err := db.Exec(sqlStatements, newProfile.Id, newProfile.Name, newProfile.Age, newProfile.Address,
		newProfile.Email, newProfile.PhoneNumber, newProfile.Hobby)
	if err != nil {
		panic(err)
	}
	fmt.Print(rest)

}

// func getconnection() *sql.DB {
// 	c := make(chan string)
// 	sqlStatements := fmt.Sprintf(`host=%s port=%d dbname=%s user=%s password=%s sslmode=disable`, host, port, dbname, user, password)
// 	db, err := sql.Open("postgres", sqlStatements)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer db.Close()
// 	err = db.Ping()
// 	if err != nil {
// 		panic(err)
// 	}
// 	fmt.Println("Successfully Connected")
// 	fmt.Print(<-c)
// 	return db

// }
